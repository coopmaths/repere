import { Graphe } from "./repere2d"
console.log(`\\documentclass[crop,tikz]{standalone}% 'crop' is the default for v1.0, before it was 'preview'
%\\usetikzlibrary{...}% tikz package already loaded by 'tikz' option
\\usepackage{pgfplots}
\\begin{document}
\\begin{tikzpicture}
\\begin{axis}[xlabel=\\(x\\),ylabel=\\(y\\)]
${new Graphe("2*x^3", [-2, 2], 100, "smooth", "red").render()}
\\end{axis}
\\end{tikzpicture}
\\end{document}
`)